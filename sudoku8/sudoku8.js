const sudoku = Array.from({ length: 81 });
const values = Array.from({ length: 9 }, (_, index) => index);
const numbers = Array.from({ length: 9 }, (_, index) => index + 1);
const sudokuBoard = document.querySelector(".sudoku");

const getLines = (prev, current, _, array) => {
  let line = [];
  const isFirst = current === 0;
  if (isFirst) {
    line = array.map((value) => value * 1);
  } else {
    line = prev[current - 1].map((value) => (value += 9));
  }
  return [...prev, line];
};

const getColumns = (prev, current, _, array) => {
  let column = [];
  const isFirst = current === 0;
  if (isFirst) {
    column = array.map((value) => value * 9);
  } else {
    column = prev[current - 1].map((value) => (value += 1));
  }
  return [...prev, column];
};

const getBlocks = (prev, current, _, array) => {
  let block = [];
  if (current === 0) {
    block = array.map((value) =>
      value < 3
        ? value
        : value % 3 === 0
        ? value * 3
        : value === 4 || value === 7
        ? 3 * value - 2
        : 3 * value - 4
    );
  } else if (current === 3 || current === 6) {
    block = prev[current - 3].map((value) => (value += 27));
  } else {
    block = prev[current - 1].map((value) => (value += 3));
  }
  return [...prev, block];
};

const lines = values.reduce(getLines, []);
const columns = values.reduce(getColumns, []);
const blocks = values.reduce(getBlocks, []);

const getLine = (index) => Math.floor(index / 9);
const getColumn = (index, line) => index - line * 9;
const getBlock = (line, column) => {
  let block = 0;
  if ([0, 1, 2].includes(line)) {
    if ([0, 1, 2].includes(column)) block = 0;
    if ([3, 4, 5].includes(column)) block = 1;
    if ([6, 7, 8].includes(column)) block = 2;
  }
  if ([3, 4, 5].includes(line)) {
    if ([0, 1, 2].includes(column)) block = 3;
    if ([3, 4, 5].includes(column)) block = 4;
    if ([6, 7, 8].includes(column)) block = 5;
  }
  if ([6, 7, 8].includes(line)) {
    if ([0, 1, 2].includes(column)) block = 6;
    if ([3, 4, 5].includes(column)) block = 7;
    if ([6, 7, 8].includes(column)) block = 8;
  }
  return block;
};
const removeDuplicated = (array) =>
  array.filter((el, i, arr) => arr.indexOf(el) === i).sort((a, b) => a - b);

const getPositionValidations = (lines, columns, blocks) =>
  sudoku.map((_, index) => {
    const line = getLine(index);
    const column = getColumn(index, line);
    const block = getBlock(line, column);
    const array = removeDuplicated([
      ...lines[line],
      ...columns[column],
      ...blocks[block],
    ]);
    return array.filter((value) => value !== index);
  });

const positionValidations = getPositionValidations(lines, columns, blocks);

const mapPositionValues = (pos) =>
  Number(document.querySelector(`#pos-${pos}`)?.value || 0);
const filterPositionValues = (pos) => pos > 0;
const getValidations = (index) =>
  positionValidations[index]
    .map(mapPositionValues)
    .filter(filterPositionValues);
const getArrayFilter = (array, validations) =>
  array.filter((number) => !validations.includes(number));
const getRandomValue = (arrayFilter) => {
  const randomIndex = Math.floor(Math.random() * arrayFilter.length);
  const randomValue = arrayFilter[randomIndex];
  return randomValue || 0;
};
const getNewArray = (array, randomValue) =>
  array.filter((number) => number !== randomValue);

const getValue = (index, array = [1, 2, 3, 4, 5, 6, 7, 8, 9]) => {
  const validations = getValidations(index);
  const arrayFilter = getArrayFilter(array, validations);
  const randomValue =
    arrayFilter.length > 1 ? getRandomValue(arrayFilter) : arrayFilter[0];
  const randomValueIsValid = !validations.includes(randomValue);
  return randomValueIsValid
    ? randomValue
    : getValue(index, getNewArray(array, randomValue));
};

function myFunction(event) {
  event.preventDefault();
  event.target.value = event.target.value.replace(/[^1-9]/g, ""); // TODO: Modificar regex para permitir a digitação de apenas 1 número entre 1 a 9.
}

const input = (index) => {
  const input = document.createElement("input");
  input.setAttribute("id", `pos-${index}`);
  input.setAttribute("type", `text`);
  input.setAttribute("class", `sudoku__item`);
  input.setAttribute("maxlength", 1);
  input.setAttribute("onkeyup", "myFunction(event)");
  return input;
};
const position = (index) => document.querySelector(`#pos-${index}`);

const printBoard = () => {
  sudokuBoard.innerHTML = "";
  sudoku.forEach((_, index) => sudokuBoard.appendChild(input(index)));
};

const savePositionValue = (index) =>
  (position(index).value = getValue(index) || "X");

const setValuesByBlock = (blockIndex) => {
  blocks[blockIndex].forEach(savePositionValue);
};

const checkValues = (size) => {
  const allValues = Object.values(document.querySelectorAll(".sudoku__item"))
    .map((item) => item.value)
    .filter((value) => value > 0);
  return allValues.length === size;
};

printBoard();

const removePositions = () => {
  blocks.forEach((block) => {
    const randomIndex = Math.floor(Math.random() * block.length);
    const randomValue = block[randomIndex];
    position(randomValue).value = "";
  });
};

let counter = 1;
let blockIndex = 0;
let deuRuim = 0;
const check = Array.from({ length: 9 }, () => false);
const perfectBoard = () => check.every((item) => item === true);

const interval = setInterval(() => {
  setValuesByBlock(blockIndex);
  const totalPreenchidos = (blockIndex + 1) * 9;
  const deuBom = checkValues(totalPreenchidos);
  if (deuBom) {
    deuRuim = 0;
    check[blockIndex] = true;
    blockIndex++;
  } else {
    deuRuim++;
    if (deuRuim === 3) {
      printBoard();
      blockIndex = 0;
    }
  }
  if (perfectBoard()) {
    console.log("deu bom!");
    removePositions();
    clearInterval(interval);
  }
  if (counter > 200) {
    console.log("deu ruim!");
    clearInterval(interval);
  }
  console.log(`tentativa ${counter++}`);
}, 100);
