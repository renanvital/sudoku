const sudoku = Array.from({ length: 81 });
const indexes = Array.from({ length: 9 }, (_, index) => index);
const numbers = Array.from({ length: 9 }, (_, index) => index + 1);
const boardGame = document.querySelector('.sudoku');

const getLines = (prev, current, _, array) => {
  let line = [];
  const isFirst = current === 0;
  if (isFirst) {
    line = array.map((value) => value * 1);
  }
  else {
    line = prev[current-1].map((value) => value += 9);
  }
  return [...prev, line];
}

const getColumns = (prev, current, _, array) => {
  let column = [];
  const isFirst = current === 0;
  if (isFirst) {
    column = array.map((value) => value * 9);
  }
  else {
    column = prev[current-1].map((value) => value += 1);
  }
  return [...prev, column];
}

const getBlocks = (prev, current, _, array) => {
  let block = [];
  if (current === 0) {
    block = array.map((value) => value < 3 ? value : value % 3 === 0 ? value * 3 : value === 4 || value === 7 ? 3 * value - 2 : 3 * value - 4)
  }
  else if (current === 3 || current === 6) {
    block = prev[current-3].map((value) => value += 27)
  }
  else {
    block = prev[current-1].map((value) => value += 3);
  }  
  return [...prev, block];
}

const lines = indexes.reduce(getLines, []);
const columns = indexes.reduce(getColumns, []);
const blocks = indexes.reduce(getBlocks, []);

const getLine = (index) => Math.floor((index / 9));
const getColumn = (index, line) => (index - ((line) * 9));
const getBlock = (line, column) => {
  let block = 0;
  if ([0,1,2].includes(line)) {
    if ([0,1,2].includes(column)) block = 0;
    if ([3,4,5].includes(column)) block = 1;
    if ([6,7,8].includes(column)) block = 2;
  }
  if ([3,4,5].includes(line)) {
    if ([0,1,2].includes(column)) block = 3;
    if ([3,4,5].includes(column)) block = 4;
    if ([6,7,8].includes(column)) block = 5;
  }
  if ([6,7,8].includes(line)) {
    if ([0,1,2].includes(column)) block = 6;
    if ([3,4,5].includes(column)) block = 7;
    if ([6,7,8].includes(column)) block = 8;
  }  
  return block;
}
const removeDuplicated = (array) => array.filter((el, i, arr) => arr.indexOf(el) === i).sort((a,b) => a-b);

const getPositionValidations = (lines, columns, blocks) => sudoku.map((_, index) => {
  const line = getLine(index);
  const column = getColumn(index, line);
  const block = getBlock(line, column);
  const array = removeDuplicated([...lines[line], ...columns[column], ...blocks[block]]);
  return array.filter((value) => value !== index);
});

const positionValidations = getPositionValidations(lines, columns, blocks);

const mapPositionValues = pos => Number(document.querySelector(`#pos-${pos}`)?.value || 0);
const filterPositionValues = pos => pos > 0;
const getValidations = index => positionValidations[index].map(mapPositionValues).filter(filterPositionValues);

const getRandomBlockIndex = (arrayBlock, number) => {
  const randomIndex = Math.floor(Math.random() * arrayBlock.length);
  const randomPosition = arrayBlock[randomIndex];
  const blockValidations = getValidations(randomPosition);
  const randomPositionIsValid = !blockValidations.includes(number);
  return randomPositionIsValid ? randomPosition : getRandomBlockIndex(arrayBlock.filter(item => item !== number), number);
}

const input = (index) => `<input disabled id="pos-${index}" class="sudoku__item" />`;
const position = (index) => document.querySelector(`#pos-${index}`);

const printBoard = () => {
  sudoku.forEach((_, index) => boardGame.innerHTML += input(index));
}

const setValuesByNumbers = () => {
  numbers.forEach((number) => {
    blocks.forEach((block) => {
      const positionIndex = getRandomBlockIndex(block, number);
      position(positionIndex).value = number;
    });
  })
}

printBoard();
setValuesByNumbers();