const sudoku = Array.from({ length: 81 });
const values = Array.from({ length: 9 }, (v, k) => v = k);

const criarColuna1 = array => array.map((value) => value * 9);
const criarColuna = coluna => coluna.map((value) => value += 1);

const criarLinha1 = array => array.map((value) => value * 1);
const criarLinha = linha => linha.map((value) => value += 9);

const criarBloco1 = (array) => array.map((value) => value < 3 ? value : value % 3 === 0 ? value * 3 : value === 4 || value === 7 ? 3 * value - 2 : 3 * value - 4);
const criarBloco = bloco => bloco.map((value) => value += 3);

const soma27 = (value) => value += 27;

const coluna1 = criarColuna1(values);
const coluna2 = criarColuna(coluna1);
const coluna3 = criarColuna(coluna2);
const coluna4 = criarColuna(coluna3);
const coluna5 = criarColuna(coluna4);
const coluna6 = criarColuna(coluna5);
const coluna7 = criarColuna(coluna6);
const coluna8 = criarColuna(coluna7);
const coluna9 = criarColuna(coluna8);

const linha1 = criarLinha1(values);
const linha2 = criarLinha(linha1);
const linha3 = criarLinha(linha2);
const linha4 = criarLinha(linha3);
const linha5 = criarLinha(linha4);
const linha6 = criarLinha(linha5);
const linha7 = criarLinha(linha6);
const linha8 = criarLinha(linha7);
const linha9 = criarLinha(linha8);

const bloco1 = criarBloco1(values);
const bloco2 = criarBloco(bloco1);
const bloco3 = criarBloco(bloco2);
const bloco4 = bloco1.map(soma27);
const bloco5 = criarBloco(bloco4);
const bloco6 = criarBloco(bloco5);
const bloco7 = bloco4.map(soma27);
const bloco8 = criarBloco(bloco7);
const bloco9 = criarBloco(bloco8);