const sudoku = Array.from({ length: 81 }, (_, index) => index === 2 ? 5 : null);
const values = Array.from({ length: 9 }, (_, index) => index);

const getLines = (prev, current, _, array) => {
  let line = [];
  const isFirst = current === 0;
  if (isFirst) {
    line = array.map((value) => value * 1);
  }
  else {
    line = prev[current-1].map((value) => value += 9);
  }
  return [...prev, line];
}

const getColumns = (prev, current, _, array) => {
  let column = [];
  const isFirst = current === 0;
  if (isFirst) {
    column = array.map((value) => value * 9);
  }
  else {
    column = prev[current-1].map((value) => value += 1);
  }
  return [...prev, column];
}

const getBlocks = (prev, current, _, array) => {
  let block = [];
  if (current === 0) {
    block = array.map((value) => value < 3 ? value : value % 3 === 0 ? value * 3 : value === 4 || value === 7 ? 3 * value - 2 : 3 * value - 4)
  }
  else if (current === 3 || current === 6) {
    block = prev[current-3].map((value) => value += 27)
  }
  else {
    block = prev[current-1].map((value) => value += 3);
  }  
  return [...prev, block];
}

const lines = values.reduce(getLines, []);
const columns = values.reduce(getColumns, []);
const blocks = values.reduce(getBlocks, []);

const validar0 = new Set([...blocks[0], ...lines[0], ...columns[0]].sort((a,b) => a-b))
const insertValue = 8;

if (!validar0.has(sudoku.indexOf(insertValue))) {
  sudoku[0] = insertValue;
}
