const sudoku = Array.from({length: 81});
const numeroMagico = 9;
const vezes = Array.from({ length: 9 });

const criarColuna = coluna => coluna.map((value) => value += 1)
const criarLinha = linha => linha.map((value) => value += 9)

const coluna1 = vezes.map((_, index) => index * 9);
const coluna2 = criarColuna(coluna1);
const coluna3 = criarColuna(coluna2);
const coluna4 = criarColuna(coluna3);
const coluna5 = criarColuna(coluna4);
const coluna6 = criarColuna(coluna5);
const coluna7 = criarColuna(coluna6);
const coluna8 = criarColuna(coluna7);
const coluna9 = criarColuna(coluna8);

console.log({ coluna1, coluna2, coluna3, coluna4, coluna5, coluna6, coluna7, coluna8, coluna9 })

const linha1 = vezes.map((_, index) => index * 1);
const linha2 = criarLinha(linha1);
const linha3 = criarLinha(linha2);
const linha4 = criarLinha(linha3);
const linha5 = criarLinha(linha4);
const linha6 = criarLinha(linha5);
const linha7 = criarLinha(linha6);
const linha8 = criarLinha(linha7);
const linha9 = criarLinha(linha8);

console.log({ linha1, linha2, linha3, linha4, linha5, linha6, linha7, linha8, linha9 })

const pegarRepetidos = (value, index, array) => {
  let result = false;
 const find = array.find((v, i) => i != index && v == value);
 if (find >= 0) result = true;
 return result;
}
const removerRepetidos = (value, index, array) => {
  let result = true;
 const findIndex = array.indexOf(value);
 if (findIndex >= 0) result = false;
 return result;
}
const bloco = [...coluna1, ...coluna2, ...coluna3, ...linha1, ...linha2, ...linha3];
const bloco1 = bloco.filter(pegarRepetidos).filter(removerRepetidos).sort((a, b) => a - b)

